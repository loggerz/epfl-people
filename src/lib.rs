use futures::future::{BoxFuture, FutureExt};
use std::fs::File;
use std::path::Path;
use std::collections::{HashMap, HashSet};
use std::io::prelude::*;

pub mod models;

use crate::models::{Unit, RawUnit, IndividualWithUnitPaths};

/// Recursive fetch of a unit
fn fetch_unit(unit_acronym: String) -> BoxFuture<'static, Result<Unit, String>> {
    println!("Fetching unit {}", unit_acronym);
    async move {
        let response = reqwest::get(format!("https://search-api.epfl.ch/api/unit?hl=fr&showall=0&siteSearch=unit.epfl.ch&q=EPFL&acro={}", unit_acronym)).await.unwrap();
        
        // Debugging input format
        // let text = response.text().await.unwrap();
        // println!("response : {}", text);
        // let raw_unit: RawUnit = serde_json::from_str(&text)
        //     .map_err(|err| format!("json err for unit {} : {:?}", unit_acronym, err))?;

        let raw_unit: RawUnit = response.json().await
            .map_err(|err| format!("json err for unit {} : {:?}", unit_acronym, err))?;

        let unit: Unit = raw_unit.build_unit().await?;
        Ok(unit)
    }.boxed()
}

pub async fn load_recursive_units() -> Result<Unit, String> {
    if Path::new("EPFL.json").exists() {
        let mut file = File::open("EPFL.json").unwrap();
        let mut string = String::new();
        let _ = file.read_to_string(&mut string).unwrap();
        println!("Loaded root unit from EPFL.json");
        Ok(serde_json::from_str(&string).unwrap())
    } else {
        let unit = fetch_unit("EPFL".to_owned()).await.unwrap();
        let mut file = File::create("EPFL.json").unwrap();
        let string: String = serde_json::to_string(&unit).unwrap();
        file.write_all(string.as_bytes()).unwrap();
        return Ok(unit)
    }
}

fn merge_maps(a: &mut HashMap::<String, IndividualWithUnitPaths>, b: HashMap::<String, IndividualWithUnitPaths>) {
    for sciper_b in b.keys() {
        let individual_b = b.get(sciper_b).unwrap();
        if let Some(individual_a) = a.get_mut(sciper_b) {
            individual_a.unit_paths.extend(individual_b.unit_paths.to_owned().into_iter());
        } else {
            a.insert(sciper_b.to_owned(), individual_b.clone());
        }
    }
}



pub fn build_people_map(root_unit: Unit) -> HashMap<String, IndividualWithUnitPaths> {
    let mut map: HashMap::<String, IndividualWithUnitPaths> = HashMap::new();
    
    if let Some(ind_vec) = root_unit.people {
        let unit_paths_set: HashSet<String> = HashSet::from([root_unit.unit_path]);
        for ind in ind_vec {
            let ind_with_units = IndividualWithUnitPaths::from_individual(&ind, unit_paths_set.clone());
            if let Some(ind_mut) = map.get_mut(&ind_with_units.sciper) {
                ind_mut.unit_paths.extend(unit_paths_set.clone());
            } else {
                map.insert(ind_with_units.sciper.clone(), ind_with_units);
            }
        }
    }

    if let Some(subunits) = root_unit.subunits {
        for subunit in subunits {
            let subunit_map = build_people_map(subunit);
            merge_maps(&mut map, subunit_map);
        }
    }

    map
}

pub async fn save_people_map(people_map: HashMap<String, IndividualWithUnitPaths>) {
    let mut file = File::create("people_map.json").unwrap();
    let string: String = serde_json::to_string(&people_map).unwrap();
    file.write_all(string.as_bytes()).unwrap();
}