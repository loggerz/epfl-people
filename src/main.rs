use epfl_people::{load_recursive_units, build_people_map, save_people_map};

#[tokio::main]
async fn main() {
    let root_unit = load_recursive_units().await.unwrap();
    let people_map = build_people_map(root_unit);
    save_people_map(people_map).await;
}
