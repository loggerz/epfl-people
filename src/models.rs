use serde::{Serialize, Deserialize};
use std::{collections::HashSet, hash::{Hash, Hasher}};
use crate::fetch_unit;

#[derive(Deserialize, Debug, Serialize, Hash)]
pub struct Individual {
    pub name: String,
    pub firstname: String,
    pub email: Option<String>,
    pub sciper: String,
}

impl PartialEq for Individual {
    fn eq(&self, other: &Self) -> bool {
        self.sciper == other.sciper
    }
}

impl Eq for Individual {}

#[derive(Deserialize, Debug)]
pub struct SubUnit {
    pub acronym: String,
    pub name: String
}

#[derive(Deserialize, Debug)]
pub struct RawUnit {
    pub code: i32,
    pub acronym: String,
    pub name: String,
    #[serde(rename = "unitPath")]
    pub unit_path: String,
    pub people: Option<Vec<Individual>>,
    pub subunits: Option<Vec<SubUnit>>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Unit {
    pub code: i32,
    pub acronym: String,
    pub name: String,
    pub unit_path: String,
    pub people: Option<HashSet<Individual>>,
    pub subunits: Option<HashSet<Unit>>
}

impl PartialEq for Unit {
    fn eq(&self, other: &Self) -> bool {
        self.code == other.code
    }
}

impl Eq for Unit {}

impl Hash for Unit {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.code.hash(state);
    }
}

impl RawUnit {
    pub async fn build_unit(self) -> Result<Unit, String> {
        let mut new_subunits: HashSet<Unit> = HashSet::new();
        if let Some(subunits) = self.subunits {
            for subunit in subunits {
                let new_subunit = fetch_unit(subunit.acronym).await?;
                new_subunits.insert(new_subunit);
            }
        }
        let mut people_set: HashSet<Individual> = HashSet::new();
        if let Some(people) = self.people {
            people_set.extend(people.into_iter());
        }
        Ok(Unit {
            code: self.code,
            acronym: self.acronym,
            name: self.name,
            unit_path: self.unit_path,
            people: match people_set.is_empty() {
                true => None,
                false => Some(people_set)
            },
            subunits: Some(new_subunits)
        })
    }
} 

#[derive(Deserialize, Debug, Serialize, Clone)]
pub struct IndividualWithUnitPaths {
    pub name: String,
    pub firstname: String,
    pub email: Option<String>,
    pub sciper: String,
    pub unit_paths: HashSet<String>
}

impl IndividualWithUnitPaths {
    pub fn from_individual(ind: &Individual, unit_paths: HashSet<String>) -> IndividualWithUnitPaths {
        IndividualWithUnitPaths {
            name: ind.name.clone(),
            firstname: ind.firstname.clone(),
            email: ind.email.clone(),
            sciper: ind.sciper.clone(),
            unit_paths
        }
    }
}